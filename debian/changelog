python-motor (2.1.0-2) unstable; urgency=medium

  * Restore new-crt.patch.

 -- Ondřej Nový <onovy@debian.org>  Sun, 12 Jan 2020 15:56:25 +0100

python-motor (2.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.

  [ Balint Reczey ]
  * Add minimal Salsa CI config
  * tests: Expect asyncio.exceptions.TimeoutError where it is present.
    This fixes test failure with Python 3.8
  * New upstream version 2.1.0
  * Drop patches integrated upstream
  * Depend on python3-pymongo (>= 3.10)

 -- Ondřej Nový <onovy@debian.org>  Sun, 12 Jan 2020 14:43:45 +0100

python-motor (2.0.0-3) unstable; urgency=medium

  * Rename d/tests/control.autodep8 to d/tests/control.
  * Bump standards version to 4.3.0 (no changes)
  * Bump debhelper compat level to 12
  * Drop Python 2 support

 -- Ondřej Nový <onovy@debian.org>  Mon, 08 Jul 2019 13:52:00 +0200

python-motor (2.0.0-2) unstable; urgency=medium

  * Regenerate test certificates, make it compatible with OpenSSL 1.1.1
  * d/tests: Require newer MongoDB server
  * Bump standards version to 4.2.1 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Sat, 29 Sep 2018 22:24:43 +0200

python-motor (2.0.0-1) unstable; urgency=medium

  * New upstream release
  * Convert git repository from git-dpm to gbp layout
  * d/p/0001-MOTOR-248-Don-t-use-async-as-variable-name.patch:
    Drop, applied upstream
  * d/tests: Bump required version of python-mockupdb
  * Bump standards version to 4.2.0 (no changes)

 -- Ondřej Nový <onovy@debian.org>  Mon, 20 Aug 2018 13:22:19 +0200

python-motor (1.2.3-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add upstream fix for compatibility with Python 3.7.
    (Closes: #903527)

 -- Adrian Bunk <bunk@debian.org>  Fri, 10 Aug 2018 23:31:48 +0300

python-motor (1.2.3-1) unstable; urgency=medium

  * New upstream release
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Bump required version of pymongo to 3.6
  * Add upstream metadata
  * Standards-Version is 4.1.4 now (no changes needed)

 -- Ondřej Nový <onovy@debian.org>  Tue, 29 May 2018 13:30:50 +0200

python-motor (1.2.1-1) unstable; urgency=medium

  * New upstream release
  * d/copyright: Bump upstream copyright year

 -- Ondřej Nový <onovy@debian.org>  Mon, 22 Jan 2018 09:41:36 +0100

python-motor (1.2.0-1) unstable; urgency=medium

  * New upstream release
  * d/rules: Don't remove aiohttp and asyncio in Python 2 (fixed upstream)
  * d/copyright: Bump my copyright year
  * Bump debhelper compat level to 11
  * Standards-Version is 4.1.3 now (no changes needed)
  * debian/tests: Run tests on all Python versions

 -- Ondřej Nový <onovy@debian.org>  Tue, 16 Jan 2018 15:32:57 +0100

python-motor (1.1-2) unstable; urgency=medium

  * Uploading to unstable

 -- Ondřej Nový <onovy@debian.org>  Tue, 20 Jun 2017 12:37:33 +0200

python-motor (1.1-1) experimental; urgency=medium

  * New upstream release
  * Bump required version of PyMongo to 3.4
  * d/copyright: Bump copyright years
  * d/s/options: Ignore .egg-info
  * Remove not needed B-D which are useful only for tests
  * Add python{,3}-gridfs and python-concurrent.futures to Depends
  * Suggests python{,3}-tornado and python3-aiohttp
  * d/tests: Run upstream unit tests

 -- Ondřej Nový <onovy@debian.org>  Sat, 08 Apr 2017 15:40:45 +0200

python-motor (1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #816969)

 -- Ondřej Nový <onovy@debian.org>  Tue, 15 Nov 2016 18:10:05 +0100
